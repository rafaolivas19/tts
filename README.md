@rafaolivas/tts
===============

Simple http service that reproduces voice based on text sent through a POST request

[![oclif](https://img.shields.io/badge/cli-oclif-brightgreen.svg)](https://oclif.io)
[![Version](https://img.shields.io/npm/v/@rafaolivas/tts.svg)](https://npmjs.org/package/@rafaolivas/tts)
[![Downloads/week](https://img.shields.io/npm/dw/@rafaolivas/tts.svg)](https://npmjs.org/package/@rafaolivas/tts)
[![License](https://img.shields.io/npm/l/@rafaolivas/tts.svg)](https://gitlab.com/rafaolivas19/tts/blob/master/package.json)

* [Installation](#installation)
* [Usage](#usage)
* [Options](#options)

# Installation

``` elm
npm install -g @rafaolivas/tts
```

# Usage

``` elm
$ tts [options]
Listening at http://computer:2754/rut
```

then do a POST request to `http://computer:2754/print` with body

``` json
{
    "text": "Hello world!"
}
```

This will play a voice in your computer with the text you sent.

# Options

``` elm
-c, --cert=cert  Path to the cert file to enable https protocol (must be provided along with key file)
-h, --help       show CLI help
-k, --key=key    Path to the key file to enable https protocol (must be provided along with cert file)
-p, --port=port  Port in wich the service will listen for requests, 2754 by default
-v, --version    show CLI version
```
