import { Command, flags } from '@oclif/command'
const say = require('say');
const express = require('express');
const cors = require('cors');
const https = require('https');
const http = require('http');
const fs = require('fs');
const os = require('os');

class Tts extends Command {
    static description = 'describe the command here'

    static flags = {
        version: flags.version({ char: 'v' }),
        help: flags.help({ char: 'h' }),
        key: flags.string({
            char: 'k',
            description: 'Path to the key file to enable https protocol (must be provided along with cert file)'
        }),
        cert: flags.string({
            char: 'c',
            description: 'Path to the cert file to enable https protocol (must be provided along with key file)'
        }),
        port: flags.integer({
            char: 'p',
            description: 'Port in wich the service will listen for requests, 2754 by default'
        })
    }

    static args = [{ name: 'file' }]

    async run() {
        this.startServer();
    }

    startServer() {
        const { flags } = this.parse(Tts)
        let app = express();
        app.use(express.json());
        const isHttpsEnabled = flags.key && flags.cert;
        const port = flags.port || 2754;
        let server: any;

        app.use(cors({
            origin: '*'
        }));

        app.get('/rut', (request: any, response: any) => {
            response.json({ status: 'ok', message: 'I\'m here' });
        });

        app.post('/say', async (request: any, response: any) => {
            const text = request.body.text;
            say.speak(text);
            response.status(200);
            response.json({ status: 'ok' });
        });

        if (isHttpsEnabled) {
            server = https.createServer(
                {
                    key: fs.readFileSync(flags.key as string),
                    cert: fs.readFileSync(flags.cert as string)
                },
                app
            )
        }
        else {
            server = http.createServer(app);
        }

        server.listen(port, () => {
            const protocol = isHttpsEnabled ? 'https' : 'http';

            console.log(
                `Listening at ${protocol}://${os.hostname().toLowerCase()}:${port}/rut`
            );
        });
    }

}

export = Tts
